#ifndef HISTCULT_H
#define HISTCULT_H

#include <iostream>
#include <string>
#include "LocaisDeInteresse.h"

using namespace std;

class HistCult : public LocaisInteresse{
private:
	double TempoMedio;
	double HorarioAbert;
	double HorarioEncerr;
public:
	//construtores:
	HistCult();
	HistCult(string descricao, double tempMedio, double horarioA, double horarioE);
	HistCult(const HistCult &l);
	~HistCult();
	//gets e sets:
	double getTempoMedio()const;
	double getHorarioAbert()const;
	double getHorarioEncerr()const;
	void setTempoMedio(double tmed);
	void setHorarioAbert(double hab);
	void setHorarioEncerr(double hen);

	//metodos ler e escrever
	void ler();
	void escrever(ostream &out)const;
	//clone
	LocaisInteresse* clone() const;
	//sobrecarga de operadores
	void operator=(const HistCult &h);
	bool operator==(const HistCult &h);

};
#endif