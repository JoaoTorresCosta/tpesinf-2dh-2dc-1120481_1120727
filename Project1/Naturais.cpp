#include "Naturais.h"

//CONSTRUTOR
Naturais::Naturais() : LocaisInteresse(){
	area = 0;
}

Naturais::Naturais(string local, double a) : LocaisInteresse(local){
	area = a;
}

Naturais::Naturais(const Naturais& nat) : LocaisInteresse(nat){
	area = nat.area;
}

//DESTRUTOR
Naturais::~Naturais(){
	
}

//ESCRITA
void Naturais::saida(ostream &out) const{
	LocaisInteresse::escreve(out);
	out << "\n�rea: " << area << endl;
}

//GET
double Naturais::getArea() const{
	return area;
}

//SET
void Naturais::setArea(double a2){
	area = a2;
}

//SOBRECARGA
ostream & operator <<(ostream &out, const Naturais &nat)
{
	nat.saida(out);
	return out;
}

ostream & operator <<(ostream &out, const Naturais *nat)
{
	nat->saida(out);
	return out;
}


//istream& operator>> (istream& is, Naturais &nat)
//{
//	is >> nat.getArea();
//	return is;
//}

void Naturais::operator=(const Naturais & nat)
{
	this->setDescri�ao(nat.getDescricao());
	this->area = nat.area;
}

bool Naturais::operator==(const Naturais & nat)
{
	string desc1 = this->getDescricao();
	string desc2 = nat.getDescricao();
	if (desc1.compare(desc2) == 0 && this->area == nat.area)//comparar a descri��o de dois objetos
	{
		return true;
	}
	else return false;
}

   LocaisInteresse* Naturais::clone() const{
	   return new Naturais(*this);
}
