#ifndef LocaisInteresse_h
#define LocaisInteresse_h

#include <iostream>
#include <string>

using namespace std;

class LocaisInteresse{
private:
	//variaveis
	string descricao;

public:
	//construtor e destrutor
	LocaisInteresse();
	LocaisInteresse(string descricao);
	LocaisInteresse(const LocaisInteresse &l);
	//obriga subclasse a conter o destrutor
	virtual ~LocaisInteresse();
	//sets
	void setDescrišao(string descricao);
	//gets
	string getDescricao() const;
	


	//rotinas de leitura e escrita (virtuais)
	virtual void escreve(ostream &out) const;
	
	
	virtual LocaisInteresse* clone()const;

	//sobrecarga de operadores
	bool operator==(const LocaisInteresse &l);
	void operator=(const LocaisInteresse &l);

};

#endif