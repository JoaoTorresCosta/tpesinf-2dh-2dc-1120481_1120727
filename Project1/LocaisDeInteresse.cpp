#include "LocaisDeInteresse.h"



//construtores:
LocaisInteresse::LocaisInteresse(){descricao = "";}
LocaisInteresse::LocaisInteresse(string local){descricao = local;}
LocaisInteresse::LocaisInteresse(const LocaisInteresse &l){descricao = l.descricao;}
//destrutor
LocaisInteresse::~LocaisInteresse(){}

//gets e sets
string LocaisInteresse::getDescricao() const{return descricao;}
void LocaisInteresse::setDescrišao(string descric){descricao = descric;}

//metodos ler e escrever:
void LocaisInteresse::escreve(ostream &o)const{
	o << "\nDescricao: " << descricao << endl;}

//sobrecarga de operadores
ostream &operator <<(ostream &out, const LocaisInteresse &l){
	l.escreve(out);
	return out;
}
ostream & operator <<(ostream &o, const LocaisInteresse *l)
{	l->escreve(o);
	return o;
}
void LocaisInteresse::operator=(const LocaisInteresse & l)
{
	this->descricao = l.descricao;
}
bool LocaisInteresse::operator==(const LocaisInteresse & l)
{	string x = this->getDescricao();
	string y = l.getDescricao();
	if (x.compare(y) == 0){return true;}
	else return false;}


//clone
LocaisInteresse* LocaisInteresse::clone() const
{
	return new LocaisInteresse(*this);
}