#ifndef NATURAIS_H
#define NATURAIS_H

#include <string.h>
#include <iostream>

using namespace std;

class Naturais: public LocaisDeInteresse{

private:
	int area;
	//a descri��o do local deriva da classe LocaisDeInteresse.h

public:
	//CONSTRUTOR
	Naturais();
	Naturais(string local, double a);//string descri��o local, double area
	Naturais(const Naturais& n);
	
	//DESTRUTOR
	~Naturais();
	
	void saida(ostream &out) const;//escrita

	//GET
	int getArea() const;
	
	//SET
	int setArea(int a2);

	//SOBRECARGA
	void operator=(const Naturais &nat);
	
	bool operator==(const Naturais &nat);

	LocaisDeInteresse* clone() const;

};
#endif